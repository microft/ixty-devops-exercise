Ixty DevOps Interview Excercise
===============================

Provision a local multi-container django-cms install with Vagrant, Docker and 
Ansible.

After running "vagrant up" I can navigate to a webpage displaying the 
django-cms 'Installation Successful' screen.

Requirements:

  1. The page should be served via NGINX.
  2. There must be two seperate containers running django-cms with a WSGI 
     compliant server of your choosing.
  3. There must be a minimum of 4 containers configured as follows:
       a. NGINX
       b. Python / WSGI
       c. Python / WSGI
       d. Postgreql 
  4. The vagrant configuration should work on Linux.
  5. Where possible, Ansible should be used for configuration.

Bonus Considerations:

  1. Portability of vagrant config i.e. make it compatible with Windows / OSX
  2. Logging - aggregation / rotation etc.
  3. Container orchestration and service discovery



After running "vagrant up" open the address http://localhost:8888.

  1. NGINX is acting as load balancer between the two djangocms containers.
  2. djangocms1 and djangocms2 are container running the same instance. Which runs uwsgi.
  3. a) nginx b)djangocms1 c)djangocms2 d)database
  4. Configuration tested on Ubuntu 14.04.2 LTS
  5. Ansible was only needed to configure the Vagrant host vm (for usage on OSX / Windows)

  1. Config runs on OSX by the way of Vagrant's host vm. (Not tested on Windows)
  2. Logging aggregation was achieved by using the logentries docker container. Logs are sent to logentries directly.
  3. Any number of djangocms containers can be launched/removed. The NGINX config will reload to add/remove node from rotation.

Considerations:

	Because there are links between the containers the command to use is 'vagrant up --no-parallel'
	For the OSX/Windows host I assumed the virtualbox provider.
	Using the host VM is smoother if you launch it before starting the 'main' vagrant. (do 'vagrant up' in the host directory)
